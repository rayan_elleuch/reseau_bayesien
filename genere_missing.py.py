# -*- coding: utf-8 -*-
"""
Created on Wed Dec 03 16:04:55 2014

@author: rAyyy
"""

import csv
import pyAgrum as gum
import random



def parentsName(bn,n): 
  return [bn.variable(parent).name() for parent in bn.parents(n)]

bn=gum.loadBN("bn.bif")


percentage_missing=0.1
potential_number_val_missing=5

#taille de la base de donnée
taille = 1000

#preparation du fichier csv
csvfile = open('bdd_missing.csv', 'wb')
csvfile.truncate()
spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

size=0
for i in bn.topologicalOrder():
    size+=1
variables=[0]*size
for elmt in range(size):
    variables[elmt]=bn.variable(elmt).name()
    
#creationde N tirage
for i in range(taille):
    
    cache={} #dictionaire possédant les valeurs relié au variable tiré
    #Obtenir la distribution de probabilité nécessaire au tirage de v
    for i in bn.topologicalOrder():
        
        #on cherch les parent et leur valeur déja tiré
        Par=parentsName(bn,i)
        truc={a:cache[a] for a in Par}
        
        #distribution de probabilité
        proba=bn.cpt(i)[truc]
        
        #tirage
        p=0
        rnd=random.random()
        for k in range(len(proba)):
            p+=proba[k]
            if rnd < p:
                cache[bn.variable(i).name()]=k #on met la valeur tiré dans le cache
                break
                
    temporaire=list(variables)
    for i in range(potential_number_val_missing):
        indx=random.randint(0, len(temporaire)-1)
        rnd=random.random()
        
        if rnd < percentage_missing:
            cache[temporaire[indx]]=100
        temporaire.remove(temporaire[indx])
    #on écrit dans le fichier csv dans le bon ordre
    spamwriter.writerow([cache[a] for a in variables])
    


csvfile.close()
    