# -*- coding: utf-8 -*-
"""
Created on Thu Dec 04 00:21:09 2014

@author: rAyyy
"""
import csv
import pyAgrum as gum
import numpy
from scipy.stats import multivariate_normal


csvfile = open('bdd_X.csv', 'rb')

spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

bn=gum.loadBN("bn_X.bif")

bn_p=gum.loadBN("bn_X.bif")


size=0
for i in bn.topologicalOrder():
    size+=1
variables=[0]*size
for elmt in range(size):
    variables[elmt]=bn.variable(elmt).name()

def childrensName(bn,n): 
  return [bn.variable(child).name() for child in bn.children(n)]

def parentsName(bn,n): 
  return [bn.variable(parent).name() for parent in bn.parents(n)]

def compareParams(b1,b2): 
    nombre_parametre=0
    s=0
    for p in bn.topologicalOrder():
        p_1=b1.cpt(p)
        p_2=b2.cpt(p)
        
        i=gum.Instantiation(p_1)
        j=gum.Instantiation(p_2)
        i.setFirst()
        j.setFirst()
        
        while (not i.end()):
            nombre_parametre+=1
            s+=(p_1.get(i) - p_2.get(j))**2
            i.inc()
            j.inc()
    
    return s / nombre_parametre
    
    
#lecture de la base de données
for p in bn_p.topologicalOrder():
        #distribution de probabilité
        p_a=bn_p.cpt(p)
        
        
        noeud=bn.variable(p)
        size= len(noeud)
        
        i=gum.Instantiation(p_a)
        i.setFirst()
        
        random_array= numpy.random.rand(size)
        random_array=numpy.array([1.0/size]*size)
        a=random_array.sum()
        random_array=random_array / a
        s=0
        compteur=0
        while (not i.end()):
            p_a.set(i,random_array[compteur])
            compteur+=1
            if compteur==size:
                compteur=0
            i.inc()
        


bn_compte=gum.loadBN("bn_X.bif")


MegaDico=[]
#lecture/comptage
for row in spamreader:
    cache={}  
    for p in bn.topologicalOrder():
        noeud = bn_compte.variable(p).name()
        cache[noeud]=row[p]
    for p in bn.topologicalOrder():
        
        
        noeud = bn_compte.variable(p).name()
        Parents=parentsName(bn,p)
        Childrens=childrensName(bn,p)
        
        temp={key: cache[key] for key in Parents}
        temp[noeud]=cache[noeud]
        
        if '100' not in temp.values():
            bn.cpt(p)[temp]+=1
        else:
            if cache[noeud]=='100':
                for child in Childrens:
                    temp[child]=cache[child]
            test=False
            for elmt in MegaDico:
                if elmt[0]==temp :
                    if  elmt[1]==p:
                        elmt[2]+=1
                        test=True
            if test == False:
                MegaDico.append([temp,p,1])






#while abs (somme1-somme2) >= 0.1:
somme_tot_curent=0
somme_tot_previous=50
nani=0

mean0=[0.5,0.5]
mean1=[0.7,2]
var0=[1,1.2]
var1=[1,1.1]
#for kop in range(40)  :
while abs (somme_tot_curent-somme_tot_previous) >= 0.1:
    nani+=1
    somme_tot_previous=somme_tot_curent
    somme_tot_curent=0  
    print somme_tot_previous
    
    X0=0
    X1=0
    Var0=0
    Var1=0
    mean_compte=[]
    for elmt in MegaDico:
        var_name=bn_compte.variable(elmt[1]).name()
        
        inconnus = [[key,0] for key, value in elmt[0].items() 
             if (value=='100'and key!=bn_compte.variable(elmt[1]).name())]
        test=False
        while test==False:
                inc_size=len(inconnus)
                #print inc_size
                some_dict = {key: value for key, value in elmt[0].items() 
                     if value!='100'and key!=bn_compte.variable(elmt[1]).name()}
                for key,value in inconnus:
                    if key!=var_name:
                        some_dict[key]=value
                        
                
                
                #on remplace les distribution de probabilité pour e et c
                if var_name=='x':
                        #print var_name
                        var = multivariate_normal(mean=mean0, cov=var0)
                        bn_p.cpt(3)[{'x':0,'c':some_dict['c']}]=var.pdf([some_dict['e'],some_dict['c']])
                        bn_p.cpt(5)[{'x':0,'e':some_dict['e']}]=var.pdf([some_dict['e'],some_dict['c']])
                        #print {'x':0,'e':some_dict['e']}
                        var = multivariate_normal(mean=mean1, cov=var1)
                        bn_p.cpt(3)[{'x':1,'c':some_dict['c']}]=var.pdf([some_dict['e'],some_dict['c']])
                        bn_p.cpt(5)[{'x':1,'e':some_dict['e']}]=var.pdf([some_dict['e'],some_dict['c']])
                        #print bn_p.cpt(5)[{'x':1}]
                        ie = gum.LazyPropagation(bn_p)
                        ie.setEvidence(some_dict)
                        ie.makeInference()
                        a=ie.posterior(0)[{}]
                        tem=numpy.array([float(some_dict['c']),float(some_dict['e'])])
                        test=True
                        for things in mean_compte:
                            if tem[0]==things[0][0] and  tem[1]==things[0][1]:
                                print a
                                things[1]+=a
                                test=False
                        if test != False:
                            mean_compte.append([tem,a*elmt[2]])
                        
                #print a
                #print bn_compte.cpt(elmt[1])[some_dict]
                
                
                
                
                
                ie = gum.LazyPropagation(bn_p)
                ie.setEvidence(some_dict)
                ie.makeInference()
                a=ie.posterior(elmt[1])[{}]
                if elmt[0][var_name] !='100':
                    #print elmt[0][var_name]
                    #print elmt[0]
                    
                    u=elmt[0][var_name]
                    some_dict[var_name]=elmt[0][var_name]
                    #print some_dict
                    #print "here",bn_compte.cpt(elmt[1])[some_dict]
                    bn_compte.cpt(elmt[1])[some_dict]+= a[int(u)]*elmt[2]
                else:
                    #print bn_compte.cpt(elmt[1])[some_dict]
                    bn_compte.cpt(elmt[1])[some_dict]+= a*elmt[2]
                    #print bn_compte.cpt(elmt[1])[some_dict]
                #print bn_compte.cpt(elmt[1])[some_dict]
                if inc_size!=0:
                    test2=False
                    for i in range(inc_size):
                        p=variables.index(inconnus[i][0])
                        #print p
                        size=len(bn.variable(p))
                        #print "size",size
                        #print inconnus[i][1]
                        if inconnus[i][1]== size-1 and i!=inc_size-1:
                            inconnus[i][1]=0
                            inconnus[i+1][1]+=1
                            test2=True
                        elif i==inc_size-1 and inconnus[i][1]== size-1:
                            test=True
                            #print "yop1"
                    if inconnus[inc_size-1][1] == len(bn.variable(variables.index(inconnus[inc_size-1][0]))):
                        test=True
                        #print "yop2"
                    if test2!=True:
                        inconnus[0][1]+=1
                        test2=False
                else:
                    test=True
                
    #calcul des nouvelles variances et moyennes
    tot0=[0,0]
    tot1=[0,0]
    for elmt in mean_compte:
        #print elmt[0]
        tot0+=elmt[0]*elmt[1][0]
        tot1+=elmt[0]*elmt[1][1]
    mean0=tot0 / bn_compte.cpt(0)[{'x':0}]
    mean1=tot1 / bn_compte.cpt(0)[{'x':1}]
    print 'mean X=0: ',mean0
    print 'mean X=1: ',mean1
    tot0=[0,0]
    tot1=[0,0]
    for elmt in mean_compte:
        tot0+=elmt[1][0]*(elmt[0]-mean0)**2
        tot1+=elmt[1][1]*(elmt[0]-mean0)**2
    var0=tot0 / bn_compte.cpt(0)[{'x':0}]
    var1=tot1 / bn_compte.cpt(0)[{'x':1}]


    #normalisation
    for p in bn_p.topologicalOrder():
        '''print "ho"
        print bn.variable(p).name()
        print bn_p.cpt(p)[{}]'''
        #distribution de probabilité
        p_p=bn_p.cpt(p)
        p_compte=bn_compte.cpt(p)
        p_a=bn.cpt(p)
        
        
        noeud=bn.variable(p)
        size= len(noeud)
        #print size
        
        i=gum.Instantiation(p_a)
        j=gum.Instantiation(p_compte)
        k=gum.Instantiation(p_p)
        i.setFirst()
        j.setFirst()
        k.setFirst()
        compteur=0
        s=0
        while (not i.end()):
            numerateur=p_compte.get(j)+p_a.get(i)
            
            p_compte.set(j,0)
            s+=numerateur
            somme_tot_curent+=numerateur
            p_p.set(k,numerateur)
            compteur+=1
            if compteur==size:
                p_p.set(k,p_p.get(k)/s)
                compteur=0
                for element in range(size-1):
                    k.dec()
                    p_p.set(k,p_p.get(k)/s)
                for element in range(size-1):
                    k.inc()
                s=0
            i.inc()
            j.inc()
            k.inc()
    print

print nani
for p in bn.topologicalOrder():
    print bn.variable(p).name()
    print bn_p.cpt(p)[{}]