# -*- coding: utf-8 -*-
"""
Created on Sat Nov 29 18:22:13 2014

@author: rAyyy
"""
import csv
import pyAgrum as gum
import random


def parentsName(bn,n): 
  return [bn.variable(parent).name() for parent in bn.parents(n)]

bn=gum.loadBN("bn.bif")


#preparation du fichier csv
csvfile = open('bdd.csv', 'wb')
csvfile.truncate()
spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

#taille de la base de donnée
taille = 1000

size=0
for i in bn.topologicalOrder():
    size+=1
variables=[0]*size
for elmt in range(size):
    variables[elmt]=bn.variable(elmt).name()

#creationde N tirage
for i in range(taille):
    
    cache={} #dictionaire possédant les valeurs relié au variable tiré
    #Obtenir la distribution de probabilité nécessaire au tirage de v
    for i in bn.topologicalOrder():
        #on cherch les parent et leur valeur déja tiré
        Par=parentsName(bn,i)
        dico={a:cache[a] for a in Par}
        #distribution de probabilité
        proba=bn.cpt(i)[dico]
        
        #tirage
        p=0
        rnd=random.random()
        for k in range(len(proba)):
            p+=proba[k]
            if rnd < p:
                cache[bn.variable(i).name()]=k #on met la valeur tiré dans le cache
                break
    #on écrit dans le fichier csv dans le bon ordre
    spamwriter.writerow([cache[a] for a in variables])
    


csvfile.close()
    