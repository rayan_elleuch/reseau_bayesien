# -*- coding: utf-8 -*-
"""
Created on Sat Nov 29 20:21:14 2014

@author: rAyyy
"""

import csv
import pyAgrum as gum

def parentsName(bn,n): 
  return [bn.variable(parent).name() for parent in bn.parents(n)]
  
def compareParams(b1,b2): 
    nombre_parametre=0
    s=0
    for p in bn.topologicalOrder():
        p_1=b1.cpt(p)
        p_2=b2.cpt(p)
        
        i=gum.Instantiation(p_1)
        j=gum.Instantiation(p_2)
        i.setFirst()
        j.setFirst()
        
        while (not i.end()):
            nombre_parametre+=1
            s+=(p_1.get(i) - p_2.get(j))**2
            i.inc()
            j.inc()
    
    return s / nombre_parametre
    
    
    
    
bn=gum.loadBN("bn_ss_parametre.bif")

print bn

csvfile = open('bdd.csv', 'rb')


spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

#on lit la base de donnée
for row in spamreader:
    
    cache={}    
    for i in bn.topologicalOrder():
        noeud = bn.variable(i).name()
        cache[noeud]=row[i]
        Parents=parentsName(bn,i)
        
        dico={parent:cache[parent] for parent in Parents}
        dico[noeud]=cache[noeud]
        bn.cpt(i)[dico]+=1
        
#normalisation
for p in bn.topologicalOrder():
    
        #distribution de probabilité
        p_a=bn.cpt(p)
        var_name=bn.variable(p).name()
        
        noeud=bn.variable(p)
        size= len(noeud)
        
        i=gum.Instantiation(p_a)
        i.setFirst()
        
        s=0
        compteur=0
        while (not i.end()):
            compteur+=1
            s+=p_a.get(i)# on compte les occurence
            
            if compteur==size:
                compteur=0 #on va passer à un nouveau ensemble de parent
                if s !=0:
                        p_a.set(i,p_a.get(i)/s) #on normalise le premier cas , var_name = 0
                for element in range(size-1):
                    i.dec()
                    if s !=0:
                        p_a.set(i,p_a.get(i)/s)#on divise les autre cas var_name=1 ou 1,2 ou etc
                for element in range(size-1):
                    i.inc()
                s=0
            
            i.inc()

for p in bn.topologicalOrder():
    print bn.variable(p).name()
    print bn.cpt(p)[{}]
 
bn2=gum.loadBN("bn.bif")
    
print "ecart type: ", compareParams(bn,bn2)