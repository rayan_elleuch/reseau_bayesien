# -*- coding: utf-8 -*-
"""
Created on Sat Dec 06 09:00:16 2014

@author: rAyyy
"""
import csv
import pyAgrum as gum
import numpy


def parentsName(bn,n): 
  return [bn.variable(parent).name() for parent in bn.parents(n)]

def childrensName(bn,n): 
  return [bn.variable(child).name() for child in bn.children(n)]
      
def compareParams(b1,b2): 
    nombre_parametre=0
    s=0
    for p in bn.topologicalOrder():
        p_1=b1.cpt(p)
        p_2=b2.cpt(p)
        
        i=gum.Instantiation(p_1)
        j=gum.Instantiation(p_2)
        i.setFirst()
        j.setFirst()
        
        while (not i.end()):
            nombre_parametre+=1
            s+=(p_1.get(i) - p_2.get(j))**2
            i.inc()
            j.inc()
    
    return s / nombre_parametre
    
    

    
bn=gum.loadBN("bn_ss_parametre.bif")

bn_p=gum.loadBN("bn_ss_parametre.bif")

bn_compte=gum.loadBN("bn_ss_parametre.bif")

size=0
for i in bn.topologicalOrder():
    size+=1
variables=[0]*size
for elmt in range(size):
    variables[elmt]=bn.variable(elmt).name()
    
#initialisation
for p in bn_p.topologicalOrder():
        #distribution de probabilité
        p_a=bn_p.cpt(p)
        
        
        noeud=bn.variable(p)
        size= len(noeud)
        #print size
        
        i=gum.Instantiation(p_a)
        i.setFirst()
        
        random_array= numpy.random.rand(size)
        a=random_array.sum()
        random_array=random_array / a
        s=0
        compteur=0
        while (not i.end()):
            p_a.set(i,random_array[compteur])
            compteur+=1
            if compteur==size:
                compteur=0
                random_array= numpy.random.rand(size)
                a=random_array.sum()
                random_array=random_array / a
                
            
            i.inc()
            

csvfile = open('bdd_missing.csv', 'rb')


spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')

MegaDico=[]
#lecture de la base de données
for row in spamreader:
    cache={}    
    for p in bn.topologicalOrder():
        noeud = bn_compte.variable(p).name()
        cache[noeud]=row[p]
    for p in bn.topologicalOrder():
        
        
        noeud = bn_compte.variable(p).name()
        Parents=parentsName(bn,p)
        Childrens=childrensName(bn,p)
        
        #la variable + les parents
        temp={key: cache[key] for key in Parents}
        temp[noeud]=cache[noeud]
        
        if '100' not in temp.values():
            #si il ne manque rien on compte
            bn.cpt(p)[temp]+=1
        else:
            if cache[noeud]=='100':
                for child in Childrens:
                    #ajout des enfants
                    temp[child]=cache[child]
            test=False
            for elmt in MegaDico:
                #si ce cas et deja présent on augmente l'occurence, sinon on l'ajoute
                if elmt[0]==temp :
                    if  elmt[1]==p:
                        elmt[2]+=1
                        test=True
            if test == False:
                MegaDico.append([temp,p,1])
    

somme_tot_curent=0
somme_tot_previous=50
nani=0
#Boucle
while abs (somme_tot_curent-somme_tot_previous) >= 0.1:
    nani+=1
    somme_tot_previous=somme_tot_curent
    somme_tot_curent=0  
    print somme_tot_previous
    
    #Estimation
    for elmt in MegaDico:
        var_name=bn_compte.variable(elmt[1]).name()
        
        inconnus = [[key,0] for key, value in elmt[0].items() 
             if (value=='100'and key!=bn_compte.variable(elmt[1]).name())]
        test=False
        while test==False:
                inc_size=len(inconnus)
                
                
                
                some_dict = {key: value for key, value in elmt[0].items() 
                     if value!='100'and key!=bn_compte.variable(elmt[1]).name()}
                for key,value in inconnus:
                    if key!=var_name:
                        some_dict[key]=value
                
                
                ie = gum.LazyPropagation(bn_p)
                ie.setEvidence(some_dict)
                ie.makeInference()
                a=ie.posterior(elmt[1])[{}]
                
                #on ajoute dans bn_compte
                if elmt[0][var_name] !='100':
                    #print elmt[0][var_name]
                    #print elmt[0]
                    
                    u=elmt[0][var_name]
                    some_dict[var_name]=elmt[0][var_name]
                    #print some_dict
                    bn_compte.cpt(elmt[1])[some_dict]+= a[int(u)]*elmt[2] 
                else:
                    bn_compte.cpt(elmt[1])[some_dict]+= a*elmt[2]
                #print bn_compte.cpt(elmt[1])[some_dict]
                    
                #cette parti permet de balayer toute les valeurs possible
                if inc_size!=0:
                    for i in range(inc_size):
                        p=variables.index(inconnus[i][0])
                        size=len(bn.variable(p))
                        if inconnus[i][1]== size-1 and i!=inc_size-1:
                            inconnus[i][1]=0
                            inconnus[i+1][1]+=1
                        elif i==inc_size-1 and inconnus[i][1]== size-1:
                            test=True
                        if inconnus[inc_size-1][1] == len(bn.variable(variables.index(inconnus[inc_size-1][0]))):
                            test=True
                    inconnus[0][1]+=1
                else:
                    test=True
                

    #normalisation
    for p in bn_p.topologicalOrder():
        p_p=bn_p.cpt(p)
        p_compte=bn_compte.cpt(p)
        p_a=bn.cpt(p)
        
        noeud=bn.variable(p)
        size= len(noeud)
        
        i=gum.Instantiation(p_a)
        j=gum.Instantiation(p_compte)
        k=gum.Instantiation(p_p)
        i.setFirst()
        j.setFirst()
        k.setFirst()
        
        compteur=0
        s=0
        while (not i.end()):
            numerateur=p_compte.get(j)+p_a.get(i)
            p_compte.set(j,0)
            s+=numerateur
            somme_tot_curent+=numerateur
            p_p.set(k,numerateur)
            compteur+=1
            if compteur==size:
                p_p.set(k,p_p.get(k)/s)
                compteur=0
                for element in range(size-1):
                    k.dec()
                    p_p.set(k,p_p.get(k)/s)
                for element in range(size-1):
                    k.inc()
                s=0
            i.inc()
            j.inc()
            k.inc()
print nani
for p in bn.topologicalOrder():
    print bn.variable(p).name()
    print bn_p.cpt(p)[{}]
bn2=gum.loadBN("bn.bif")

print "ecart type: ", compareParams(bn_p,bn2)